// Styles
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'
import { md, aliases } from 'vuetify/iconsets/md'

export default createVuetify({
    icons: {
        sets: { md },
        defaultSet: "md",
        aliases,
    },
})