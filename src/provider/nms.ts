import { Provider } from ".";
import { IngredientSetting, RecipeInfo, UiInfo } from "@/store";

import pLimit from 'p-limit';

import { float, int, letter, ParsingState, string, whitespace } from "parjs";
import { defineCombinator, many, maybe, or, qthen, stringify, then, thenq } from "parjs/combinators"
import { ParjserBase } from "parjs/internal/parser";

const REFINER_TIME = "Refiner Time";
const REFINER_UI_INFO = (() => {
    const info = new UiInfo;
    info.image = "https://static.wikia.nocookie.net/nomanssky_gamepedia/images/6/66/Buildable.refiner3.png";
    return info;
})();

function named(type: string) {
    return defineCombinator((source: ParjserBase) => {
        return new (class Called extends ParjserBase {
            type = type;
            expecting = source.expecting;

            _apply(ps: ParsingState) {
                source.apply(ps);
            }
        })();
    });
}


const RESOURCE = /{{(?:Resource|Product) infobox([^}]*)}}/
const RESOURCE_KV = /\s([^=\s]*)\s*=\s*([^|]*)\s/g

const REFINE = /{{PoC-Refine([^}]*)}}/

const STRING =
    letter().pipe(
        or(" ", "-"),
        many(),
        stringify()
    );

const VAL_AMOUNT = named("Val, Ammount")(
    STRING
        .pipe(
            thenq(string(",")),
            then(int()),
        )
)

const REFINE_LINE = named("RefineLine")(
    VAL_AMOUNT
        .pipe(
            thenq(string(";")),
            many(),
            then(
                named("Qte")(int().pipe(thenq(string(";")))),
                named("Time per unit")(float()),
                named("Optional name")(string("%").pipe(
                    qthen(STRING),
                    maybe()
                )),
            ),
        )
)

const REFINE_LIST = named("RefineList")(
    whitespace().pipe(
        qthen(
            string("|").pipe(
                named("PIPE"),
                named("BetweenSpace"),
                qthen(REFINE_LINE),
                thenq(whitespace()),
                many(),
            )
        )
    )
)

const CRAFT = /{{Craft\|([^}]*)}}/
const CRAFT_LINE = named("CraftLine")(
    STRING.pipe(
        thenq(string(",")),
        then(int()),
        thenq(string(";").pipe(maybe())),
        many(),
        then(int().pipe(maybe())),
        thenq(string("|blueprint=yes").pipe(maybe()))
    )
)

const API = "https://nomanssky.fandom.com/api.php";

export class NMS extends Provider {
    async Search(name: string): Promise<Array<string>> {
        type O = [
            string, // Search
            Array<string>, // Page title
            Array<string>, // Snippet
            Array<string>, // URL
        ]
        const o = await this.ApiFetch(`${API}?action=opensearch&search=${name}`)
        const json = await o.json() as O;

        return json[1]
    }

    static url(res: string): string {
        return `${API}?action=parse&page=${res.replace(' ', '_')}&prop=wikitext&format=json`
    }

    limiter = pLimit(1);
    PendingInfo = 0;

    async ApiFetch(url: string): Promise<Response> {
        const v = this.limiter(() => fetch(`https://api.allorigins.win/raw?url=${encodeURIComponent(url)}`));

        this.PendingInfo = this.limiter.pendingCount;
        const o = await v;
        this.PendingInfo = this.limiter.pendingCount;

        return o;
    }

    async GetImageUrl(img: string): Promise<string | null> {
        type O = {
            query: {
                pages: {
                    [key: number]: {
                        pageid: number, ns: number, title: string, imagerepository: string,
                        imageinfo: [
                            { url: string, descriptionurl: string, descriptionshorturl: string }
                        ]
                    }
                }
            }
        };

        const req = await this.ApiFetch(`${API}?action=query&titles=File:${img}&prop=imageinfo&iiprop=url&format=json`);
        const out = await req.json() as O;

        for (const pid in out.query.pages) {
            const e = out.query.pages[pid];
            if (e.title.endsWith(img)) {
                return e.imageinfo[0].url.replace(/\/revision\/.*/, '');
            }
        }
        return null;
    }


    async InitIngredient(ingredient: string, settings: IngredientSetting) {
        if (ingredient == "") return;
        if (ingredient == REFINER_TIME) {
            settings.arduousness = 0;
            settings.uiInfo = REFINER_UI_INFO
            return;
        }


        type ResourceInfo = {
            category: string,
            color: string,
            image: string,
            rarity: string,
            release: string,
            symbol: string,
            type: string,
            used: string,
            value: string,
        };

        const req = await this.ApiFetch(NMS.url(ingredient));
        const out = await req.json();
        const val = out.parse.wikitext['*'];

        const res = RESOURCE.exec(val)
        const rawResource: Record<string, string> = {};
        if (res) {
            for (const e of res[1].matchAll(RESOURCE_KV)) {
                rawResource[e[1]] = e[2];
            }
        }
        const resource = rawResource as ResourceInfo;

        this.GetImageUrl(resource.image).then(o => {
            settings.uiInfo.image = o;
        })
    }

    async FetchRelatedCraft(ingredient: string): Promise<Array<RecipeInfo>> {
        const recipes = [];

        const req = await this.ApiFetch(NMS.url(ingredient));
        const out = await req.json();
        const val = out.parse.wikitext['*'];

        const refine = REFINE.exec(val)
        if (refine) {
            const o = REFINE_LIST.parse(refine[1])
            if (o.isOk) {
                for (const e of o.value) {
                    const recipe = new RecipeInfo
                    recipe.name = e[3]
                    recipe.to.list[0].name = ingredient
                    recipe.to.list[0].qte = e[1]

                    recipe.from.list[0].name = REFINER_TIME;
                    recipe.from.list[0].qte = e[2]
                    for (const f of e[0]) {
                        recipe.from.add()
                        const idx = recipe.from.list.length - 1;
                        recipe.from.list[idx].name = f[0]
                        recipe.from.list[idx].qte = f[1]
                    }

                    recipes.push(recipe)
                }
            } else {
                console.error(o.toString(), o)
            }
        }

        const craft = CRAFT.exec(val);
        if (craft) {
            const o = CRAFT_LINE.parse(craft[1])
            if (o.isOk) {
                const value = o.value;
                const recipe = new RecipeInfo
                recipe.to.list[0].name = ingredient
                recipe.to.list[0].qte = value[1] ?? 1;

                recipe.from.list.pop();
                for (const f of value[0]) {
                    recipe.from.add()
                    const idx = recipe.from.list.length - 1;
                    recipe.from.list[idx].name = f[0]
                    recipe.from.list[idx].qte = f[1]
                }

                recipes.push(recipe)
            } else {
                console.error(o.toString(), o)
            }
        }

        return recipes
    }
}
