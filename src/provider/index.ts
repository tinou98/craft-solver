import { IngredientSetting, RecipeInfo } from "@/store";
import { ref } from "vue";

export abstract class Provider {
    abstract InitIngredient(ingredient: string, settings: IngredientSetting): void;
    abstract FetchRelatedCraft(ingredient: string): Promise<Array<RecipeInfo>>;
    abstract Search(name: string): Promise<Array<string>>

    abstract PendingInfo: number;
}

export const MainProvider = ref<Provider>();