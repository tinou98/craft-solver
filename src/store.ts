import { reactive, ref, watch } from "vue"
import { MainProvider } from "./provider"

export class Reactive {
    constructor() {
        return reactive(this);
    }
}

export class IngredientInfo extends Reactive {
    id = Symbol("IngredientInfo")
    name = ""
    qte = 1

    toString() {
        let out = this.name
        if (this.qte != 1)
            out = this.qte + " x " + out
        return out
    }
}

export class IngredientListInfo extends Reactive {
    list: Array<IngredientInfo> = [new IngredientInfo]

    add() { this.list.push(new IngredientInfo) }
    delete(s: symbol) { this.list = this.list.filter(e => e.id !== s) }
}

export class RecipeInfo extends Reactive {
    id = Symbol("RecipeInfo")
    name?: string
    from: IngredientListInfo = new IngredientListInfo
    to: IngredientListInfo = new IngredientListInfo
}

export class RecipeListInfo extends Reactive {
    list: Array<RecipeInfo> = []

    add() { this.list.push(new RecipeInfo) }
    delete(s: symbol) { this.list = this.list.filter(e => e.id !== s) }
    find(s: symbol): RecipeInfo | undefined {
        return this.list.find(e => e.id == s)
    }
}

export class UiInfo extends Reactive {
    image: string | null = null;
}

export class IngredientSetting extends Reactive {
    constructor(private name: string) {
        super()
        this.timeout = setTimeout(() => this.init(), 1000)
    }

    stop() {
        clearTimeout(this.timeout)
    }

    private init() {
        MainProvider.value?.InitIngredient(this.name, this);
        if (recursive.value) this.addRelatedCraft();
    }

    async addRelatedCraft() {
        const newRecipe = await MainProvider.value?.FetchRelatedCraft(this.name);
        if (newRecipe)
            recipes.list.push(...newRecipe);
    }

    initQte = 0;
    arduousness: number | null = null;
    qteWanted = 0;

    timeout: number;
    uiInfo = new UiInfo;
}

export const recipes = new RecipeListInfo
export const ingredients = reactive<Map<string, IngredientSetting>>(new Map);
export const recursive = ref(false);

watch(recipes, r => {
    const existingIng = new Set(ingredients.keys())
    const allIng = new Set(r.list.flatMap(e => [...e.from.list, ...e.to.list]).flatMap(e => e.name))

    function difference<T>(setA: Iterable<T>, setB: Iterable<T>): Set<T> {
        const difference = new Set(setA);
        for (const elem of setB) {
            difference.delete(elem);
        }
        return difference;
    }

    for (const e of difference(existingIng, allIng)) {
        ingredients.get(e)?.stop();
        ingredients.delete(e)
    }

    for (const e of difference(allIng, existingIng)) {
        ingredients.set(e, new IngredientSetting(e))
    }
})
