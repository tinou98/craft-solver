import { loadModule, Model, Variable } from "glpk-ts";
import { StatusSimplex } from "glpk-ts/dist/status";

import { recipes, ingredients, RecipeInfo } from "./store"


export const Module =
    loadModule(
        new URL("glpk-wasm/wasm", import.meta.url).toString());

//  1 var per recipe
// ~1 var per ingredient
type Out = {
    model: Model;
    recipe: Map<symbol, Variable>,
    must_farm: Map<string, Variable>
}

export async function buildModel(): Promise<Out> {
    await Module;

    const must_farm = new Map;
    const model = new Model;

    // Vars: nbReceipe
    const recipeVar: Array<[RecipeInfo, Variable]> = recipes.list.map(r => [r, model.addVar({
        name: r.name,
        type: "int",
        lb: 0,
    })])

    for (const [ingName, ingConfig] of ingredients) {
        /*
        OVERALL QTE                                                           >= CONSUMED + WANTED
        initQte + SUM( r in reciepe: nbReceipe * qteProduced ) + mustFarm     >= SUM(r in reciepe: nbReciepe * qteConsumed) + qteRequested
        SUM( r in reciepe: nbReceipe * (qteProduced - qteConsume)) + mustFarm >= qteRequested - initQte
        */

        const coeffs: Array<[Variable, number]> = recipeVar.map(([conf, va]) => [
            va,
            (conf.to.list.find(el => el.name == ingName)?.qte ?? 0)
            - (conf.from.list.find(el => el.name == ingName)?.qte ?? 0)
        ])

        // Add amount to farm
        if (ingConfig.arduousness !== null) {
            const create = model.addVar({
                name: "mustFarm_" + ingName,
                type: "int",
                obj: ingConfig.arduousness,
                lb: 0,
            });
            must_farm.set(ingName, create)
            coeffs.push([create, 1]);
        }
        const c = model.addConstr({
            name: ingName,
            coeffs,
            lb: ingConfig.qteWanted - ingConfig.initQte
        });
        c.add
    }

    return {
        model,
        must_farm,
        recipe: new Map(recipeVar.map(([conf, va]) => [conf.id, va])),
    }
}

export type RunResult = {
    status: StatusSimplex,
    recipe: Map<symbol, number>,
    must_farm: Map<string, number>;
}

function nonNull([, v]: [unknown, Variable]) {
    return v.value >= 1;
}

export async function runModel(): Promise<RunResult> {
    const { model, recipe, must_farm } = await buildModel()
    model.simplex()
    return {
        status: model.status,
        recipe: new Map([...recipe].filter(nonNull).map(([k, v]) => [k, Math.round(v.value)])),
        must_farm: new Map([...must_farm].filter(nonNull).map(([k, v]) => [k, Math.round(v.value)])),
    }
}