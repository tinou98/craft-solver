import { createApp } from 'vue'

import Vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'

import App from './App.vue'

loadFonts();

createApp(App)
    .use(Vuetify)
    .mount('#app')
