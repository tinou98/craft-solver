const { defineConfig } = require('@vue/cli-service')
const { VuetifyLoaderPlugin } = require('vuetify-loader')

module.exports = defineConfig({
  publicPath: new URL(process.env.CI_PAGES_URL || "http://localhost").pathname,
  transpileDependencies: true,
  chainWebpack: (config) => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap((options) => {
        return {
          ...options,
          reactivityTransform: true
        }
      })

    config.plugin('VuetifyLoaderPlugin')
      .use(VuetifyLoaderPlugin)
  },
  configureWebpack: {
    resolve: {
      fallback: {
        fs: false,
        crypto: false,
        path: false,
      },

    }
  }
})
